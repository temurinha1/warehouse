<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * @method static where(string $string, string $string1)
 * @method static create(array $array)
 */


class Material extends Model
{
    use HasFactory;

    protected $fillable = ['material_name', 'remain', 'price'];
}
