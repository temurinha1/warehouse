<?php

namespace App\Http\Controllers;

use App\Models\Material;

class MaterialsController extends Controller
{
    public function index()
    {
        // Ma'lumotlarni olish
        echo $fabricForPants = Material::where('material_name', 'ткань-1')->first();

        $fabricForShirts = Material::where('material_name', 'ткань-2')->first();
        $thread = Material::where('material_name', 'нить')->first();
        $buttons = Material::where('material_name', 'пуговица')->first();
        $zipper = Material::where('material_name', 'замок')->first();

        // Hisoblash uchun kerakli o'zgaruvchilarni aniqlash
        $fabricForPantsQuantity = 20 * 1.4;
        $fabricForShirtsQuantity = 30 * 0.8;
        $threadQuantity = 20 * 10;
        $buttonsQuantity = 30 * 5;
        $zipperQuantity = 20;

        // Qolgan material miqdorini hisoblash
        $remainingFabricForPants = $fabricForPants->remain - $fabricForPantsQuantity;
        $remainingFabricForShirts = $fabricForShirts->remain - $fabricForShirtsQuantity;
        $remainingThread = $thread->remain - $threadQuantity;
        $remainingButtons = $buttons->remain - $buttonsQuantity;
        $remainingZipper = $zipper->remain - $zipperQuantity;

        // Javob obyektini tuzish
        $response = [
            'fabric_for_pants' => $remainingFabricForPants,
            'fabric_for_shirts' => $remainingFabricForShirts,
            'thread' => $remainingThread,
            'buttons' => $remainingButtons,
            'zipper' => $remainingZipper,
        ];

        // JSON formatida javob qaytarish
        return response()->json($response);
    }
}
