<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Material;
Material::all();


class MaterialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(): void
    {
        Material::create([
            'material_name' => 'ткань-1',
            'remain' => 12,
            'price' => 1500,
        ]);

        Material::create([
            'material_name' => 'ткань-2',
            'remain' => 200,
            'price' => 1600,
        ]);

        Material::create([
            'material_name' => 'нить',
            'remain' => 40,
            'price' => 500,
        ]);

        Material::create([
            'material_name' => 'нить',
            'remain' => 300,
            'price' => 550,
        ]);

        Material::create([
            'material_name' => 'пуговица',
            'remain' => 500,
            'price' => 300,
        ]);

        Material::create([
            'material_name' => 'замок',
            'remain' => 1000,
            'price' => 20000,
        ]);
    }
}
